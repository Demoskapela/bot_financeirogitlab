const low = require('lowdb');
const config = require('./config.json');

const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync(config.lbDbName);
const db = low(adapter);

module.exports = {
    insertDb: async function(item){
        await this.initDb();
        let result = await db.get('data')
                                .find({path:item.path}).value();
        if (!result)
            return await db.get('data')
                    .push(item)
                    .write();
    },
    alterDB: async function(item){
        await this.initDb();
        return await db.get('data')
                                .find({id:item.id})
                                .assign(item).write();
    },
    deleteDb:async function(item){
        await this.initDb();
        return await db.get('data')
                                .find({id:item.id})
                                .pop().write();
    },
    searchDb: async function(id)
    {
        await this.initDb();
        return await db.get('data')
                        .find({id:id})
                        .value();
    },
    searchDbDispAlterPrice: async function(disp,alterPrice)
    {
        await this.initDb();
        return await db.get('data')
                        .filter({disponivel:disp,alterPrice:alterPrice})
                        .value();
    },
    initDb: async function(){
        let ExistData = await db.get('data').value();
        if (!ExistData)
        {
            db.defaults({ data: [] })
            .write();
        }
    }
};