const cheerio = require('cheerio');
var Decimal = require('decimal.js');

const browserHeadersKabum={
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'pt-BR,pt;q=0.9',
    'cache-control': 'no-cache',
    'cookie': '__utmz=other; smeventsclear_6c396631471543109651f9d3c059e71d=true; _hjid=63b03278-dcdb-49e2-b468-a3ab86ec9837; G_ENABLED_IDPS=google; session=2c169bad392709bb75706f0b943da257; _gcl_au=1.1.327330.1609329367; _fbp=fb.2.1613996643659.18233143; AdoptId=9edfcca5-7aa0-4abb-a49d-14a45eaff26e; _gid=GA1.3.273915361.1615818095; teste=1; smViewOnSite=true; smClosedOnSite_d268b5372b834cec9763d86a8d833569=true; smCloseOnSite=true; __udf_j=23afec472a430ed604ac6f41282ba46d8935a45f4c3ab656a671cc1952007a6b8fdb78fad12910eb2b879330d2e77011; _gac_UA-2140951-1=1.1611165520.CjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; GTMUtmTimestamp=1616619736260; GTMUtmSource=google; GTMUtmMedium=cpc; GTMUtmCampaign=auto; GTMGclid=CjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; GTMCampaignReferrer=; GTMCampaignLP=https%3A%2F%2Fwww.kabum.com.br%2Fproduto%2F147801%2Fplaca-de-video-asus-tuf-rtx3060-o12g-gaming-90yv0gc0-m0na00%3Fgclid%3DCjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; _gcl_aw=GCL.1616619736.CjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; _gac_UA-2140951-12=1.1616619737.CjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; _gac_UA-2140951-10=1.1616619739.CjwKCAjwxuuCBhATEiwAIIIz0Sg6SXlFTNMZJekZJ7p5DuiOlq55tRkMN1TA-M4Vz3jkDojQAMNfXhoCXuMQAvD_BwE; __utmc=10378415; visitor_source=direct; visitor_campaign=direct; visitor_medium=direct; _hjTLDTest=1; 4ba84402b1a3c0e68d0989e442f72a75=77c527df19d3e281995728861fca3fb1; rcc_visitor.type=; rcc_visitor.id_cliente=; rcc_visitor.city=; rcc_visitor.state=; __utmz=10378415.1616687236.262.170.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _rtbhouse_source_=organic; __utma=10378415.119102644.1593631477.1616691726.1616695128.264; __utmt=1; email_blackfriday=1; 26e6f173f9f0fb447318277c42a1aff3={"cashback":"1%","cashback_category":false}; goto=/hardware/placa-de-video-vga; __utmb=10378415.4.10.1616695128; _uetsid=bf751f80859911eba35d69df3b502a7d; _uetvid=471d4ab0709911eb89559d4da488401e; GTMGAHitCounter_UA-2140951-12=4; GTMGAHitCounter_UA-2140951-10=20; _ga_Q68S4NB67S=GS1.1.1616695128.176.1.1616695153.35; _ga=GA1.1.119102644.1593631477; _aw_m_17729=17729_1616610371_2960144dc81e4977cd6d0fd0e6c1d831; datadome=BVcu9DQEX994ocp_NkzH4P72YVscXg38Xwx8yUxTUur~oXI~.CCTQZvErWJcp4E0JzbcJvSPGC7GuNu1cdv4e_Qo7-9F9COoB2UgD.1Gfr; meliuz_coupon=28',
    'pragma': 'no-cache',
    'referer': 'https://www.kabum.com.br/hardware/placa-de-video-vga?pagina=1&ordem=5&limite=100&prime=false&marcas=[]&tipo_produto=[]&filtro=[]',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'
    };
const browserHeadersPichau={
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
    };

const browserHeadersTerabyte={
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'pt-BR,pt;q=0.9',
        'cache-control': 'no-cache',
        'cookie': '_ga=GA1.3.1385190165.1613508923; _tnd=1613508923049; __auc=037a53c6177aca04f21adaa5f22; _hjid=c1048b81-5de9-4533-8202-c2926be9236a; __bid=7f607a22-bfcb-43e0-8571-45c3ccdb509c; _gcl_aw=GCL.1613995744.CjwKCAiAyc2BBhAaEiwA44-wW5e4J5xzu4uEnx_Z1KzuGp2J5xuSfYAzUVJmRf6nuGXUtkEdqbsWFhoC_2YQAvD_BwE; _fbp=fb.2.1613996166429.316558748; smeventsclear_f9fed1c5c2354273ba8ec7d7c7183a69=true; __cfduid=d4bff58d501d8c977bfefb4d230f6f4581616155800; _tnt=eLI27Pu9U1Y3QLLJoVbwoJiXY2dEckju; __udf_j=e9ff50d708e2efeb8d524751b9da9faae33151c3800d1f0aed79c03c9667eba4b47d649ab7e8c5436eb4a66ff20e4050; _gac_UA-51700491-1=1.1616519795.Cj0KCQjwo-aCBhC-ARIsAAkNQisUkHmdg-aJM6XkiVUorX7WNJPc3PvzmIjGI3IofZnbhKuoX6LtXjMaAu2jEALw_wcB; PHPSESSID=sbiqne5ek72dds8sbacvojup07; _gid=GA1.3.1183484387.1618340377; _tnwc=s=m|m=i|a=|d=; _hjTLDTest=1; viewedOuibounceModal_1ae1398c-b1a0-4ffc-8183-4cf7b7863e5a=true; smViewBounce=true; smCloseBounce=true; __asc=de6380dc178cff3cc396c9f52f2; __cf_bm=1b9d56d8cef58958c0db8444bcd26db06025f840-1618396564-1800-Aey4me2GzfrHYWuKMLajvgvbSvwmoYtQdAXAssoXljPPwRDZfp+OpOVg+wtdDaq3//Iv8epJdaBSTg7IvL9en1H58AQsIvLU3u4hub7EjGiHtRbLCJt4MC28I3oXOIyXeZTihVFTmAe8xX0VNJbVqpesNws64VAEz60CP0rTUlEP5bCuxOu7hTxPlKzyNpCRFQ==; _hjIncludedInSessionSample=0',
        'pragma': 'no-cache',
        'referer': 'https://www.terabyteshop.com.br/',
        'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
    };

const browserHeadersNoxusit={
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'pt-BR,pt;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Cookie': '_ga=GA1.3.1955763682.1614636922; _gcl_aw=GCL.1618087807.Cj0KCQjwmcWDBhCOARIsALgJ2QfX3yTwoKeeIsf0fhOiiX9yvoIZzbHUTIO_AuumL7FUyjD-vWkjUpAaAttMEALw_wcB; _gac_UA-190775103-1=1.1618087808.Cj0KCQjwmcWDBhCOARIsALgJ2QfX3yTwoKeeIsf0fhOiiX9yvoIZzbHUTIO_AuumL7FUyjD-vWkjUpAaAttMEALw_wcB; _gid=GA1.3.842579792.1618254460; owa_v=cdh%3D%3Eace8edd3%7C%7C%7Cvid%3D%3E1614636922347988382%7C%7C%7Cfsts%3D%3E1614636922%7C%7C%7Cdsfs%3D%3E44%7C%7C%7Cnps%3D%3E32; owa_s=cdh%3D%3Eace8edd3%7C%7C%7Clast_req%3D%3E1618401448%7C%7C%7Csid%3D%3E1618401412332369066%7C%7C%7Cdsps%3D%3E0%7C%7C%7Creferer%3D%3E%28none%29%7C%7C%7Cmedium%3D%3Edirect%7C%7C%7Csource%3D%3E%28none%29%7C%7C%7Csearch_terms%3D%3E%28none%29',
        'Host': 'www.noxusit.com.br',
        'Pragma': 'no-cache',
        'Referer': 'https://www.noxusit.com.br/placas-de-video',
        'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
    }

const browserHeadersAeroluz={
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
'Accept-Encoding': 'gzip, deflate, br',
'Accept-Language': 'pt-BR,pt;q=0.9',
'Cache-Control': 'max-age=0',
'Connection': 'keep-alive',
'Cookie': '_ga=GA1.3.2135040262.1614629844; pps_times_showed_100=1; moove_gdpr_popup=%7B%22strict%22%3A%221%22%2C%22thirdparty%22%3A%221%22%2C%22advanced%22%3A%221%22%7D; _gac_UA-96755321-1=1.1616684028.CjwKCAjw6fCCBhBNEiwAem5SO4UJPQlabyJ03g3amVysbR4rsCd3mTjhB2LzXykXrhk_fQ87gQFo3xoCawcQAvD_BwE; _gcl_aw=GCL.1616684028.CjwKCAjw6fCCBhBNEiwAem5SO4UJPQlabyJ03g3amVysbR4rsCd3mTjhB2LzXykXrhk_fQ87gQFo3xoCawcQAvD_BwE; pps_show_100=Mon%20Apr%2005%202021%2014%3A22%3A26%20GMT-0300%20%28Hor%E1rio%20Padr%E3o%20de%20Bras%EDlia%29; _gid=GA1.3.678438999.1618254455; wp_woocommerce_session_6d6377c88ed71625bb104c0cd4c7ebce=4186%7C%7C1618489076%7C%7C1618485476%7C%7Cd026706461e18ea6656225bd71cd5694; woocommerce_recently_viewed=28560',
'Host': 'www.aeroluz.com.br',
'If-Modified-Since': 'Wed, 14 Apr 2021 17:55:49 GMT',
'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
'sec-ch-ua-mobile': '?0',
'Sec-Fetch-Dest': 'document',
'Sec-Fetch-Mode': 'navigate',
'Sec-Fetch-Site': 'none',
'Sec-Fetch-User': '?1',
'Upgrade-Insecure-Requests': '1',
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36'
}

module.exports = {
    PercentGood: function(valueMin,valueMax,percent){
        var percentGood=0;
        
        let valueCalc = (valueMax>valueMin?valueMax-valueMin:0);
        let percentCalc = (percent/100);

        percentGood = valueCalc*percentCalc;

        return percentGood;
    },
    SearchKabum: async function(html){
        const $ = cheerio.load(html);

        const selector = '#titulo_det > h1';
        const title = $(selector).text();
        const selectorPrice = '#pag-detalhes > div.boxs > div.box_comprar > div.box_preco > div.preco_traco > span > span > span > strong';
        const strPrice = $(selectorPrice).text();
        const price = (strPrice && strPrice!='' ?new Decimal(parseFloat(strPrice.replace('R$','').replace('.','').replace(',','.'))):new Decimal(0));
        const selectorBotaoAvise = '#pag-detalhes > div.boxs > div.box_comprar > div.box_topo > div.box_botao > p > a'
        const blnButton = $(selectorBotaoAvise).attr('onclick');

        return {title:title,price:price,blnButton:blnButton};
    },
    SearchPichau: async function(html){
                const $ = cheerio.load(html);

                const selector = '#maincontent > div.columns > div > div.product-info-main > div.product.title > h1';
                const title = $(selector).text();
                const selectorPrice = '#maincontent > div.columns > div > div.product-info-main > div.product-info-price > div.price-box.price-final_price > span.price-boleto > span';
                const strPrice = $(selectorPrice).text();
                const price = (strPrice && strPrice!='' ?new Decimal(parseFloat(strPrice.replace('à vista R$','').replace('.','').replace(',','.'))):new Decimal(0));
                const selectorBotaoAvise = '#maincontent > div.columns > div > div.product-info-main > div.product-info-price > div.product-info-stock-sku > div.stock.unavailable > span'
                const blnButton = ($(selectorBotaoAvise).val()=='Produto disponível'?false:true);
                var response = {title:title,price:price,blnButton:blnButton};
                return response;
    },
    SearchTerabyteShop: async function(html){
        const $ = cheerio.load(html);

        const selector = '#maincontent > div.columns > div > div.product-info-main > div.product.title > h1';
        const title = $(selector).text();
        const selectorPrice = '#maincontent > div.columns > div > div.product-info-main > div.product-info-price > div.price-box.price-final_price > span.price-boleto > span';
        const strPrice = $(selectorPrice).text();
        const price = (strPrice && strPrice!='' ?new Decimal(parseFloat(strPrice.replace('à vista R$','').replace('.','').replace(',','.'))):new Decimal(0));
        const selectorBotaoAvise = '#maincontent > div.columns > div > div.product-info-main > div.product-info-price > div.product-info-stock-sku > div.stock.unavailable > span'
        const blnButton = ($(selectorBotaoAvise).val()=='Produto disponível'?false:true);
        var response = {title:title,price:price,blnButton:blnButton};
        return response;
    },
    SearchNoxusit: async function(html,SKU){
        const $ = cheerio.load(html);

        const selector = '#corpo > div.conteiner > div.secao-principal.row-fluid.sem-coluna > div > div:nth-child(1) > div:nth-child(2) > div > div.info-principal-produto > h1';
        const title = $(selector).text();
        const selectorPrice = `#corpo > div.conteiner > div.secao-principal.row-fluid.sem-coluna > div > div:nth-child(1) > div:nth-child(2) > div > div.acoes-produto.disponivel.SKU-${SKU} > div:nth-child(1) > div > span > strong`;
        const strPrice = $(selectorPrice).text();
        //console.log("strPrice: ",strPrice);
        const price = (strPrice && strPrice!='' ?new Decimal(parseFloat(strPrice.replace('à vista R$','').replace('R$','').replace('.','').replace(',','.'))):new Decimal(0));
        const selectorBotaoAvise = `#corpo > div.conteiner > div.secao-principal.row-fluid.sem-coluna > div > div:nth-child(1) > div:nth-child(2) > div > div.acoes-produto.disponivel.SKU-${SKU} > div.comprar`
        const blnButton = ($(selectorBotaoAvise).attr('class')!='comprar'?true:false);
        var response = {title:title,price:price,blnButton:blnButton};
        return response;
    },
    SearchAeroluz: async function(html){
        const $ = cheerio.load(html);

        const selector = '#product-28560 > div.product-summary-wrap > div > div.summary.entry-summary.col-md-7 > h2';
        const title = $(selector).text();
        const selectorPrice = `#product-28560 > div.product-summary-wrap > div > div.summary.entry-summary.col-md-7 > div.fswp_in_cash_price.single > p > span.woocommerce-Price-amount.amount > bdi`;
        const strPrice = $(selectorPrice).text();
        //console.log("strPrice: ",strPrice);
        const price = (strPrice && strPrice!='' ?new Decimal(parseFloat(strPrice.replace('à vista R$','').replace('R$','').replace('.','').replace(',','.'))):new Decimal(0));
        const selectorBotaoAvise = `#product-28560 > div.product-summary-wrap > div > div.summary.entry-summary.col-md-7 > div.product_meta > span > span`
        const blnButton = ($(selectorBotaoAvise).text()!='Fora de estoque'?true:false);
        var response = {title:title,price:price,blnButton:blnButton};
        return response;
    },
    selectSite: async function(urlBase,html,parameter){
        switch(urlBase.split('.')[1].toLowerCase())
        {
            case "kabum":
                {
                    return this.SearchKabum(html);
                    break;
                }
            case "pichau":
                {
                    return this.SearchPichau(html);
                    break;
                }
            case "terabyteshop":
                {
                    return this.SearchTerabyte(html);
                    break;
                }
            case "noxusit":
                {
                    return this.SearchNoxusit(html,parameter);
                    break;
                }
            case 'aeroluz':
                {
                    return this.SearchAeroluz(html,parameter);
                    break;
                }
        }
    },
    selectSiteHeaders: async function(urlBase){
        switch(urlBase.split('.')[1].toLowerCase())
        {
            case 'kabum':
                {
                    return browserHeadersKabum;
                }
            case 'pichau':
                {
                    return browserHeadersPichau;
                }
            case 'terabyteshop':
                {
                    return browserHeadersTerabyte;
                }
            case 'noxusit':
                {
                    return browserHeadersNoxusit;
                }
            case 'aeroluz':
                {
                    return browserHeadersAeroluz;
                }
        }
    }

};