module.exports = async(client,msg,args)=>{
    const channel = msg.channel;
    
    if(!msg.member.hasPermission('MENAGE_MESSAGES'))
        return msg.reply('Você não tem permissão de Gerenciar Mensagens para usar esse comando!!!');
    //console.log('*** args: ',args);
    const deleteCount = parseInt(args,10);
    //console.log('*** deleteCount: ',deleteCount);
    if (!deleteCount || deleteCount<1 || deleteCount>100)
        return  msg.reply('forneça um número de 1 até 100 mensagens a serem excluídas');

    //console.log("Antes awaitMessages");
    const FetchMsg = await channel.messages.fetch({limit:deleteCount+1});
    //console.log('*** messages: ',FetchMsg);
    await channel.bulkDelete(FetchMsg);
    
    msg.reply(`Chat limpo!!! ${args} mensagem limpas nesse chat!!!!`)
    .catch(error=>console.log(`Não foi possível deletar mensagens devido a : ${error}`));
};