const fs = require('fs');
const dir ='./commands/';

module.exports=()=>{
    var commands = {};

    const scripts = fs.readdirSync(dir);
    scripts.forEach(script=>{
        commands[script.split('.')[0]] = require('../'+dir+script)
    });
    return commands;
};