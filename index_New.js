const Discord = require('discord.js');
const fs= require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const moment = require('moment');
const nodemailer = require('nodemailer');
const schedule = require('node-schedule');
const handlebars = require('handlebars');
var Decimal = require('decimal.js');
var DomParser = require('dom-parser');
var parser = new DomParser();
const linq = require('linq');
const config = require('./config.json');
const lbDB = require('./lbDB.js');
const util = require('./function.js');
const https = require('https');

const product = require("./class/product");

const commands = require('./scripts/commandsReader')();
console.log(commands);


const user = 'snikcobra2@gmail.com';
const userTo = 'ricardo.brundo@gmail.com';
//const userToCC = 'ricardo.brundo@gmail.com';
const pass='lo051151';
const PorcentBom = 25;//25%

const client= new Discord.Client();


client.login(config.token);


client.on('ready',()=>{
    
    console.log(`Bot foi iniciado, com ${client.users.size} usuários, em ${client.channels.size} canais, em ${client.guilds.size} servidores.`);
    //client.user.setPresence({game:{name:'comando',type:1,url:'https://www.twitch.tv'}});
    
    //var dateDafault = new Date('1970-01-01Z00:00:00:000');
    ReadDataJson('monitorHardware.json').then(
        response=>{
            lstMonitorHardware = response.data;
            return;
        }

    );

    console.log('*** estou pronto para ser usado.');

    schedule.scheduleJob('1 * * * * *', function(fireDate){
        console.log(`**** --> schedule Vivo - Data: ${moment(new Date()).format('DD/MM/YYYY HH:mm:SS')}!!!!!!`);
        var lstHardwareLocal = new Array();
        lstHardwareLocal = [];
        let countHardware =1;
        
        let request = https
        .get('https://servicespub.prod.api.aws.grupokabum.com.br/catalog/v1/products-by-category/hardware/placa-de-video-vga?page_number=1&page_size=20&facet_filters=&sort=&include=gift', (res) => {
            let data="";
            res.on("data",chuck=>{
                data +=chuck;
            });

            res.on("end",()=>{
                let url = JSON.parse(data);
                console.log(url)
            })
        })
        .on("error",err=>{
            console.log("Error:"+ err.message)
        });

        /*lstMonitorHardware.forEach((ItemMonitorHardware)=>
        {
            getCachePage(ItemMonitorHardware).then(response =>{
                return getPageItems(response,ItemMonitorHardware);
            })
            .then((data) => {
                countHardware = countHardware+1;
                lstHardwareLocal.push(data);
                if(lstHardwareLocal.length>0)
                {
                    saveDataJson(lstHardwareLocal,'./db.json');
                }

                //linq
                if(lstHardwareLocal && lstHardwareLocal.length>0 && lstMonitorHardware.length==lstHardwareLocal.length)
                {
                    
                    const promisesCallback = async (resolve,reject)=>{
                        var _subject = '**** Hardware alteração de preço!!!!!!';
                        
                        var filenameBody = 'templateMailBody.html';
                        const templateBody = await readFromFile(filenameBody);
                        var htmlSendBody = '';
                        //console.log('****** Antes searchDbDispAlterPrice');
                        let _lstHardwareSendMail = await lbDB.searchDbDispAlterPrice(true,true);
                        if(linq.from(_lstHardwareSendMail).where(c=>c.typePrice==1).any())
                            _subject = '**** Menor preço Hardware !!!!!!';

                        if(linq.from(_lstHardwareSendMail).where(c=>c.typePrice==2).any())
                            _subject = '**** Maior preço Hardware !!!!!!';

                        if(linq.from(_lstHardwareSendMail).where(c=>c.typePrice==3).any())
                            _subject = '**** Alerta preço próximo do Ideal - Hardware !!!!!!';

                        _lstHardwareSendMail.forEach((item)=>{

                            var hTemplateBody = handlebars.compile(templateBody); 
                            hTemplateBody = hTemplateBody(item);
                            htmlSendBody = htmlSendBody.concat(hTemplateBody);


                            const promisesSeachCallback = async (resolve,reject)=>{
                                let response = await lbDB.searchDb(item.id);
                                if(response)
                                {
                                    console.log('alterando alterprice false');
                                    item.alterPrice=false;
                                    await lbDB.alterDB(item);
                                }
                                resolve(item);
                            };
                            new Promise(promisesSeachCallback);

                        });
                        if(_lstHardwareSendMail.length>0)
                        {
                            console.log('Existe peças para enviar qtd:',_lstHardwareSendMail.length);
                            SendMail(_subject,htmlSendBody);
                        }
                        resolve(true);
                    };
                    return new Promise(promisesCallback);
                }
                else
                {
                    if(countHardware == lstMonitorHardware.length)
                        console.log('Não existe peças para enviar');
                }

            })
            .catch(console.error);
        });*/

        
    });

})

client.on("guildCreate", guild=>{
    console.log(`O Bot entrou nos servidor: ${guild.name} (id: ${guild.id}). População: ${guild.memberCount} membros!`);
    client.user.setActivity(`Estou em ${client.guilds.size} servidores.`);
    db.set(guild.id,[]).write();
});

client.on("guildDelete", guild=>{
    console.log(`O Bot foi removido do servidor: ${guild.name} (id: ${guild.id}).`);
    client.user.setActivity(`serving ${client.guild.size} servers`);
});

client.on('message', async msg=>{

    
    if(msg.author.bot) return;
    if (msg.channel.type ==="dm") return;
    if(!msg.content.startsWith(config.prefix)) return;

    console.log(`${msg.author.username}: ${msg.content}`);
    //const args = msg.content.split(' ');
    const args = msg.content.slice(config.prefix.length).trim().split(/ +/g);
    //console.log("**** args2: ",args);
    const parameter = args && args.length>1?args[1].toLowerCase():0;
    if(commands[args[0].toLowerCase()]) await commands[args[0].toLowerCase()](client,msg,parameter);

    //if(msg.content==='oi')
    //    msg.reply('Olá Bem vindo ao nosso canal de monitoramento de peças para PC.')
});

const slug  = (str) => {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
};

const stringToHTML = function (str) {
	var doc = parser.parseFromString(str, 'text/html');
	return doc.body;
};

const readFromFile = (filename)=>{
    const promisesCallback = async (resolve,reject)=>{
        await fs.readFile(filename,{encoding:'utf8'},(error,contents)=>{
            if(error)
            {
                resolve(null);
            }
            resolve(contents);
        });
    };
    return new Promise(promisesCallback);
};

const writeTofile = (data,filename) =>{
    
    const promisesCallback = (resolve,reject)=>{
        if (fs.existsSync(filename)){
            fs.unlinkSync(filename);
        }
        fs.writeFile(filename,data,(error)=>{
            if(error)
            {
                reject(error);
                return;
            }
            resolve(true);
        });
    };
    return new Promise(promisesCallback);
};

const getCachePage = (itemMonitor)=>{

    const filename = `${slug(itemMonitor.path)}.html`;
    const promisesCallback = async (resolve,reject)=>{
        const url = itemMonitor.urlBase + itemMonitor.path;
        const empresaname = itemMonitor.urlBase.split('.')[1];
        const cacheHtml = await readFromFile('pages/'+empresaname + '/'+filename);
        //if(!cacheHtml)
        {
            let _headers = await util.selectSiteHeaders(itemMonitor.urlBase);
            const html = await getPage(url,_headers);
            //await writeTofile(html,'pages/'+ empresaname + '/'+filename);
            resolve(html);
            return;
        }
        resolve(cacheHtml);
    };
    return new Promise(promisesCallback);
};

const getPage = (url,headers)=>{
    
    const options={
        headers:headers,
    };

    return axios.get(url,options).then((response)=> response.data);
};

const getPageItems = (html,ItemMonitorHardware)=>{

    const url = ItemMonitorHardware.urlBase + ItemMonitorHardware.path;
    const $ = cheerio.load(html);
    const promisesCallback = async (resolve,reject)=>
    {
        let dataSite = await util.selectSite(ItemMonitorHardware.urlBase,html,ItemMonitorHardware.path);

        const promisesSeachCallback = async (resolve,reject)=>{
            let itemDb =await lbDB.searchDb(ItemMonitorHardware.id);
            if(!itemDb)
            {
               resolve(null);
            }

            if(itemDb)
            {
                let valueGood = new Decimal(util.PercentGood(itemDb.minPrice,itemDb.maxPrice,25));
                let priceGoodPercent = new Decimal(itemDb.minPrice).add(valueGood);
                console.log(`Id: ${itemDb.id} - priceGoodPercent: ${priceGoodPercent}`);
                if(ItemMonitorHardware.price==0 && !itemDb)
                {
                    ItemMonitorHardware.typePrice = 0;
                    ItemMonitorHardware.price = ItemMonitorHardware.minPrice = ItemMonitorHardware.maxPrice=new Decimal(price);
                    ItemMonitorHardware.dateMin =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                    ItemMonitorHardware.dateMax =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                }
                else
                {
                    ItemMonitorHardware.dateMin =itemDb.dateMin;
                    ItemMonitorHardware.dateMax =itemDb.dateMax;
                    ItemMonitorHardware.price = (dataSite.price!=0?dataSite.price:0);

                    /*console.log('ItemMonitorHardware.minPrice: ',ItemMonitorHardware.minPrice);
                    console.log('ItemMonitorHardware.maxPrice: ',ItemMonitorHardware.maxPrice);
                    console.log('ItemMonitorHardware.price: ',ItemMonitorHardware.price);
                    console.log('itemDb.price: ',itemDb.price );
                    console.log('Antes das Regras');*/
                    

                    if(ItemMonitorHardware.price>itemDb.minPrice 
                        && ItemMonitorHardware.price<itemDb.maxPrice)
                    {
                        ItemMonitorHardware.typePrice = 0;
                        ItemMonitorHardware.alertColor='#FFFFFF';
                        ItemMonitorHardware.minPrice = new Decimal(itemDb.minPrice);
                        ItemMonitorHardware.maxPrice = new Decimal(itemDb.maxPrice);
                    }
                    if(ItemMonitorHardware.price < priceGoodPercent)
                    {
                        ItemMonitorHardware.typePrice = 3;
                        ItemMonitorHardware.alertColor='#ffff02';
                        ItemMonitorHardware.minPrice = new Decimal(itemDb.minPrice);
                        ItemMonitorHardware.maxPrice = new Decimal(itemDb.maxPrice);
                    }
                    if(ItemMonitorHardware.price<itemDb.minPrice)
                    {
                        ItemMonitorHardware.typePrice = 1;
                        ItemMonitorHardware.minPrice = ItemMonitorHardware.price;
                        ItemMonitorHardware.alertColor='#4BFF53';
                        ItemMonitorHardware.dateMin =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                        ItemMonitorHardware.maxPrice = new Decimal(itemDb.maxPrice);
                    }
                    if(ItemMonitorHardware.price>itemDb.maxPrice)
                    {
                        ItemMonitorHardware.typePrice = 2;
                        ItemMonitorHardware.maxPrice = ItemMonitorHardware.price;
                        ItemMonitorHardware.alertColor='#FF0000';
                        ItemMonitorHardware.dateMax =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                        ItemMonitorHardware.minPrice = new Decimal(itemDb.minPrice);
                    }

                }

                /*console.log('ItemMonitorHardware.minPrice: ',ItemMonitorHardware.minPrice);
                console.log('ItemMonitorHardware.maxPrice: ',ItemMonitorHardware.maxPrice);
                console.log('ItemMonitorHardware.price: ',ItemMonitorHardware.price);
                console.log('itemDb.price: ',itemDb.price );
                console.log('depois das Regras');*/

                if(itemDb && (ItemMonitorHardware.price > itemDb.price 
                || ItemMonitorHardware.price < itemDb.price))
                {
                    ItemMonitorHardware.alterPrice=true;
                }
                else
                {
                    ItemMonitorHardware.alterPrice=false;
                }
            }
            else
            {
                if(ItemMonitorHardware.price===0)
                {
                    ItemMonitorHardware.typePrice = 0;
                    ItemMonitorHardware.price = ItemMonitorHardware.minPrice = ItemMonitorHardware.maxPrice=new Decimal(dataSite.price);
                    ItemMonitorHardware.dateMin =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                    ItemMonitorHardware.dateMax =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                }
                else
                {

                    if(ItemMonitorHardware.price>ItemMonitorHardware.minPrice 
                        && ItemMonitorHardware.price<ItemMonitorHardware.maxPrice)
                    {
                        ItemMonitorHardware.typePrice = 0;
                        ItemMonitorHardware.alertColor='#FFFFFF';
                    }
                    if(ItemMonitorHardware.price < priceGoodPercent)
                    {
                        ItemMonitorHardware.typePrice = 3;
                        ItemMonitorHardware.alertColor='#ffff02';
                    }
                    if(ItemMonitorHardware.price<ItemMonitorHardware.minPrice)
                    {
                        ItemMonitorHardware.typePrice = 1;
                        ItemMonitorHardware.minPrice = ItemMonitorHardware.price;
                        ItemMonitorHardware.alertColor='#4BFF53';
                        ItemMonitorHardware.dateMin =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                    }
                    if(ItemMonitorHardware.price>ItemMonitorHardware.maxPrice)
                    {
                        ItemMonitorHardware.typePrice = 2;
                        ItemMonitorHardware.maxPrice = ItemMonitorHardware.price;
                        ItemMonitorHardware.alertColor='ff0000';
                        ItemMonitorHardware.dateMax =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');
                    }
                    ItemMonitorHardware.price = new Decimal((dataSite.price!=0?dataSite.price:0));
                }
                ItemMonitorHardware.alterPrice=true;
            }
        };
        new Promise(promisesSeachCallback);
        ItemMonitorHardware.disponivel=(dataSite.blnButton =='' || dataSite.blnButton ==undefined);
        ItemMonitorHardware.title =dataSite.title;
        ItemMonitorHardware.urlFull = url
        ItemMonitorHardware.date =moment(new Date()).format('DD/MM/YYYY HH:mm:SS');

        /*console.log('**************************************************************************');
        console.log('*** Date : ', ItemMonitorHardware.date);
        console.log('*** Title : ', ItemMonitorHardware.title);
        console.log('*** Price: ', ItemMonitorHardware.current + ' '+ ItemMonitorHardware.price);
        console.log('*** PriceMin: ', ItemMonitorHardware.current + ' '+ ItemMonitorHardware.minPrice);
        console.log('*** PriceMax: ', ItemMonitorHardware.current + ' '+ ItemMonitorHardware.maxPrice);
        console.log('*** CssColor: ', ItemMonitorHardware.alertColor);
        console.log('*** Disponivel: ', ItemMonitorHardware.disponivel);
        console.log('*** AlterPrice: ', ItemMonitorHardware.alterPrice);
        console.log('*** Url: ', url);
        console.log('**************************************************************************');*/

        resolve(ItemMonitorHardware);
        return;
    };
    return new Promise(promisesCallback);
};

const saveDataJson=(data,path)=>{
    const promisesCallback = async (resolve,reject)=>{
        if(!data || data.length===0) return resolve(true);
        const dataToStore = JSON.stringify({data:data},null,2);

        //const created = await writeTofile(dataToStore,path);
        //const created = lbDb
        //console.log('*** antes de inserir no lbDB.');
        //console.log(lbDB);
        await data.forEach((item)=>{
            //console.log("****item for:",item)
            const promisesSeachCallback = async (resolve,reject)=>{
                let response =await lbDB.searchDb(item.id);
                if(!response)
                {
                    await lbDB.insertDb(item);
                }
                else
                {
                    await lbDB.alterDB(item);
                }
                //console.log('responsesearch',response);
                resolve(response);
            };
            new Promise(promisesSeachCallback);
        });
        //const created = lbDB.insertDb(dataToStore.data);
        //console.log('*** depois de inserir no lbDB.');
        resolve(true);
    };
    return new Promise(promisesCallback);
};


const ReadDataJson=(path)=>{
    const promisesCallback = async (resolve,reject)=>{
        if(fs.existsSync(path))
        {
            var response = await readFromFile(path)
            resolve(JSON.parse(response));
        }
        else
            resolve(null);
    };
    return new Promise(promisesCallback);
};

const SendMail = (subject,body)=>
{
    console.log('*** Iniciando SendMail');
    const transporter = nodemailer.createTransport({
        host:'smtp.gmail.com',
        port:587,
        auth:{user,pass},
    });
    
    transporter.sendMail({
        from: user,
        to: userTo,
        replyTo: user,
        subject: subject,
        html: body//,
        //amp:body

    }).then(info=>{
        //res.send(info);
        //console.log(info);
    }).catch(error =>{
        console.log(error);
        //res.send(error);
    });
    console.log('*** Finalizado SendMail');
};